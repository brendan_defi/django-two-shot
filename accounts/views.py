from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User
from django.contrib import messages
from accounts.forms import LoginForm, SignUpForm


# Login View
def user_login(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            user = authenticate(
                request,
                username=username,
                password=password
            )

            if user is not None:
                login(request, user)
                return redirect("home")

    else:
        form = LoginForm()

    context = {
        "form": form
    }

    return render(request, "registration/login.html", context)


# Logout Function
def user_logout(request):
    logout(request)

    return redirect("login")


# Signup Function
def user_signup(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            # if the passwords match, we can move forward
            if password == password_confirmation:

                user = User.objects.create_user(
                    username,
                    password=password,
                )

                login(request, user)

                return redirect("home")

            # otherwise the passwords don't match, need to show an error
            else:
                context = {
                    "form": form
                }

                messages.add_message(request, messages.ERROR,
                                        "the passwords do not match")
                return render(request, "registration/signup.html", context)

    else:
        form = SignUpForm()

    context = {
        "form": form
    }

    return render(request, "registration/signup.html", context)


# def user_signup(request):
#     if request.method == "POST":
#         form = SignUpForm(request.POST)
#         if form.is_valid():
#             username = form.cleaned_data["username"]
#             password = form.cleaned_data["password"]
#             password_confirmation = form.cleaned_data["password_confirmation"]

#             # if this username is not being used then we can move forward
#             if not User.objects.filter(username=username).exists():

#                 # if the passwords match, we can move forward
#                 if password == password_confirmation:

#                     user = User.objects.create_user(
#                         username,
#                         password=password,
#                     )

#                     login(request, user)

#                     return redirect("home")

#                 # otherwise the passwords don't match, need to show an error
#                 else:
#                     context = {
#                         "form": form
#                     }

#                     messages.add_message(request, messages.ERROR,
#                                          "the passwords do not match")
#                     return render(request, "registration/signup.html", context)

#             # otherwise the username is in use, need to show an error
#             else:
#                 context = {
#                     "form": form
#                 }

#                 messages.add_message(request, messages.ERROR,
#                                      """This username is already in use.
#                                      Please choose another username.""")
#                 return render(request, "registration/signup.html", context)

#     else:
#         form = SignUpForm()

#     context = {
#         "form": form
#     }

#     return render(request, "registration/signup.html", context)
