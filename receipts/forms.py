from django.forms import ModelForm
from receipts.models import Receipt, ExpenseCategory, Account


# Form to Create a new Receipt
class ReceiptForm(ModelForm):
    def __init__(self, owner=None, *args, **kwargs):
        super(ReceiptForm, self).__init__(*args, **kwargs)
        if owner is not None:
            self.fields["category"].queryset = \
                ExpenseCategory.objects.filter(owner=owner)
            self.fields["account"].queryset = \
                Account.objects.filter(owner=owner)

    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
        ]


# Form to create a new Category
class ExpenseCategoryForm(ModelForm):
    class Meta:
        model = ExpenseCategory
        fields = [
            "name",
        ]


# Form to create a new Account
class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = [
            "name",
            "number",
        ]


# class ReceiptForm(ModelForm):
#     class Meta:
#         model = Receipt
#         fields = [
#             "vendor",
#             "total",
#             "tax",
#             "date",
#             "category",
#             "account",
#         ]
