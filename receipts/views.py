from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm


# Function to View All Receipts
@login_required
def receipts_list(request):
    receipts_list = Receipt.objects.filter(purchaser=request.user.id)
    receipts_list = receipts_list.order_by("-date")

    context = {
        "receipts_list": receipts_list
    }

    return render(request, "receipts/receipts_list.html", context)


# Function to View Category-specific Receipts
@login_required
def category_list(request):
    category_list = ExpenseCategory.objects.filter(owner=request.user.id)

    context = {
        "category_list": category_list
    }

    return render(request, "receipts/category_list.html", context)


# Function to View Category-specific Receipts
@login_required
def account_list(request):
    account_list = Account.objects.filter(owner=request.user.id)

    context = {
        "account_list": account_list
    }

    return render(request, "receipts/account_list.html", context)


# Function to Create a new Receipt
@login_required
def create_receipt(request):
    user = request.user
    if request.method == "POST":
        form = ReceiptForm(user, request.POST)

        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()

            return redirect("home")

    else:
        form = ReceiptForm(user)

    context = {
        "form": form
    }

    return render(request, "receipts/receipts_create.html", context)


# Function to Create a new Expense Category
@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)

        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()

            return redirect("category_list")

    else:
        form = ExpenseCategoryForm()

    context = {
        "form": form
    }

    return render(request, "receipts/category_create.html", context)


# Function to Create a new Account
@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)

        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()

            return redirect("account_list")

    else:
        form = AccountForm()

    context = {
        "form": form
    }

    return render(request, "receipts/account_create.html", context)
